QR code Test Case
=================

###Steps to install###
* add `www` directory as `DocumentRoot` of a virtual host
* composer init
* get QR code from url `http://your.virthost/get`

###Code standard###

Because the last 3 years I work with `PSR-2` not `ZF Code standard` the provided test case is under `PSR-2`.