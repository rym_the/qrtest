<?php
namespace QR;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $eventManager->attach(MvcEvent::EVENT_FINISH, array($this, 'setContent'), 100);
        $eventManager->attach(MvcEvent::EVENT_RENDER, array($this, 'disableRender'), 100);
    }

    /**
     * Listener to set correct output of content.
     * @param MvcEvent $event
     */
    public function setContent(MvcEvent $event)
    {
        $content = $event->getResponse()->getContent();
        if (is_array($content) && !empty($content['type'])) {
            $header = $event->getResponse()->getHeaders()->get('Content-Type');
            if (!empty($header)) {
                $event->getResponse()->getHeaders()->removeHeader($header);
            }
            $event->getResponse()->getHeaders()->addHeaders(array("Content-Type:{$content['type']}"));
            $content = !empty($content['data']) ? $content['data'] : $content;
            $event->getResponse()->setContent($content);
        }
        return;
    }

    /**
     * Listener to disable standard ZF renderer.
     * @param MvcEvent $event
     */
    public function disableRender(MvcEvent $event)
    {
        $event->stopPropagation(true);
        return;
    }

    /**
     * Configuration for the module.
     * @return array
     */
    public function getConfig()
    {
        return array(
            'router' => array(
                'routes' => array(
                    'get' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route' => '/get',
                            'defaults' => array(
                                'controller' => 'QRController',
                                'action' => 'get',
                            ),
                        ),
                    ),
                ),
            ),
            'service_manager' => array(
                'invokables' => array(
                    'QrCode' => 'QR\QRCode',
                    'QrRenderer' => 'QR\GoogleQR',
                ),
            ),
            'controllers' => array(
                'invokables' => array(
                    'QRController' => 'QR\Controller\QRController'
                ),
            ),
            'view_manager' => array(),
        );
    }

    /**
     * Set correct path for autoloader.
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
}