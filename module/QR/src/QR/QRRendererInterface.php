<?php

namespace QR;

/**
 * Interface QRRendererInterface
 * Interface for QR generators.
 */
interface QRRendererInterface
{
    /**
     * Generate QR-code.
     * @return mixed
     */
    public function generate();

    /**
     * Initialize generator.
     * @param $data
     * @return bool
     */
    public function init($data);
}