<?php

namespace QR;

use Zend\Http\Client;

class GoogleQR implements QRRendererInterface
{
    /**
     * @var int Height of QR code.
     */
    protected $height;

    /**
     * @var int Width of QR code.
     */
    protected $width;

    /**
     * @var string Text of QR code.
     */
    protected $text;

    /**
     * @inheritdoc
     */
    public function init($data)
    {
        foreach (array('height', 'width', 'text') as $param) {
            if (!isset($data[$param])) {
                throw new \Exception("Need {$param}");
            }
            $this->$param = $data[$param];
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function generate()
    {
        $client = new Client();
        $client->setMethod('get');
        $client->setUri('https://chart.googleapis.com/chart');
        $data = array(
            'chl' => $this->text,
            'chs' => "{$this->height}x{$this->width}",
            'cht' => "qr",
        );
        $client->setParameterGet($data);
        $response = $client->send();
        return $response->getContent();
    }
}