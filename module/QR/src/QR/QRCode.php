<?php

namespace QR;

class QRCode
{
    /**
     * @var int Height of QR code.
     */
    protected $height;

    /**
     * @var int Width of QR code.
     */
    protected $width;

    /**
     * @var string Text of QR code.
     */
    protected $text;

    /**
     * @var QRRendererInterface Renderer.
     */
    protected $renderer;

    /**
     * Set dimensions for QR code.
     * @param int $height
     * @param int $width
     */
    public function setDimensions($height, $width = -1)
    {
        $this->height = $height;
        $this->width = ($width == -1) ? $this->height : $width;
    }

    /**
     * Set text for QR code.
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * Set renderer of QR code.
     * @param QRRendererInterface $renderer
     */
    public function setRenderer(QRRendererInterface $renderer)
    {
        $this->renderer = $renderer;
    }

    /**
     * Generate QR code.
     * @return mixed
     */
    public function generate()
    {
        $this->renderer->init(array(
            'height' => $this->height,
            'width' => $this->width,
            'text' => $this->text,
        ));
        return $this->renderer->generate();
    }
}