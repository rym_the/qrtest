<?php
namespace QR\Controller;

use Zend\Mvc\Controller\AbstractActionController;

class QRController extends AbstractActionController
{
    public function getAction()
    {
        $qrCode = $this->getServiceLocator()->get('QrCode');
        $qrCode->setText('TrekkSoft');
        $qrCode->setDimensions(250, 250);
        $qrCode->setRenderer($this->getServiceLocator()->get('QrRenderer'));
        $data = $qrCode->generate();
        $this->getResponse()->setContent(
            array('data' => $data, 'type' => 'image/png')
        );
    }
}